#include "not_a_number_exception.h"

using namespace core;

not_a_number_exception::not_a_number_exception(const number_state state)
{
	this->state_ = state;
}

not_a_number_exception::number_state not_a_number_exception::get_state() const
{
	return state_;
}
