#include "Functions.h"
#include <cmath>

double core::functions::to_degrees(const double rad)
{
	return rad * 180 / pi;
}

double core::functions::to_radians(const double deg)
{
	return deg * pi / 180;
}

double core::functions::calc_trajectory_friction_derived_phi(const double x, double phi, const double v_0,
	const double m, const double profile, const double c_w, const double density)
{
	phi = to_radians(phi);
	const auto c = 0.5 * profile * c_w * density / (m * g);
	const auto x_u = calc_x_u(phi, v_0, c);

	const auto s = sin(phi)*sqrt(c)*v_0;
	const auto a = atan(s);
	const auto a_d = cos(phi) * sqrt(c) * v_0 / (1 + pow(v_0 * sin(phi), 2) * c);
	const auto b = (exp(c * g * x) - 1) / (cos(phi) * sqrt(c) * v_0);
	const auto b_d = tan(phi) * (exp(c * g * x) - 1) / (cos(phi) * sqrt(c) * v_0);

	if (x <= x_u)
		return (tan(a) * a_d - tan(a - b) * (a_d - b_d)) / (c * g);
	return (a_d * (s - 1) + b_d - 2 * (b_d - a_d) / (1 + 1 / exp(2 * (b - a)))) / (c * g);
}

double core::functions::calc_t_u(const double phi_rad, const double v_0, const double c)
{
	return atan(sin(phi_rad) * sqrt(c) * v_0) / (sqrt(c) * g);
}

double core::functions::calc_c(const double profile, const double c_w, const double density, const double m)
{
	return 0.5 * profile * c_w * density / (m * g);
}

double core::functions::calc_x_u(const double phi_rad, const double v_0, const double c)
{
	return log(cos(phi_rad) * sqrt(c) * v_0 * atan(sin(phi_rad) * sqrt(c) * v_0) + 1) / (c * g);
}

double core::functions::calc_angle_idealized(const double x, const double y, const double y_0, const double v_0, const bool alternative)
{
	const auto a = g * pow(x / v_0, 2);
	const auto b = pow(y, 2) + pow(y_0, 2) - 2 * y * y_0 + pow(x, 2);
	const auto c = pow(g, 2) * pow((x / v_0), 4);

	const auto var1 = to_degrees(
		acos(
			sqrt((
				-a * y + a * y_0 + pow(x, 2) - sqrt(pow((a * y - a * y_0 - pow(x, 2)), 2) - b * c)
				) / (
					2 * b
					))
		));

	const auto var2 = to_degrees(
		acos(
			sqrt((
				-a * y + a * y_0 + pow(x, 2) + sqrt(pow((a * y - a * y_0 - pow(x, 2)), 2) - b * c)
				) / (
					2 * b
					))
		));

	if (alternative) {
		const auto var = calc_angle_idealized(x, y, y_0, v_0, false);
		if (var == var1)
			return var2;
		return var1;
	}

	if (isnan(var1))
		return var2;
	if (isnan(var2))
		return var1;

	if (abs(45 - var1) < abs(45 - var2))
		return var1;
	return var2;
}

double core::functions::calc_trajectory_friction(const double x, double phi, const double y_0, const double v_0, const double m, const double profile,
	const double c_w, const double density)
{
	phi = to_radians(phi);
	const auto c = 0.5 * profile * c_w * density / (m * g);
	const auto a = (exp(c * g * x) - 1) / (cos(phi) * sqrt(c) * v_0);
	const auto b = atan(sin(phi) * sqrt(c) * v_0);
	const auto d = cos(b);

	const double x_u = calc_x_u(phi, v_0, c);

	if (x <= x_u) {
		return log(cos(b - a) / d) / (c * g) + y_0;
	}
	return -(log((exp(2 * (a - b)) + 1) * d / 2) - a + b) / (c * g) + y_0;
}

double core::functions::calc_height_friction(const double t, double phi, const double y_0, const double v_0, const double m,
	const double profile, const double c_w, const double density)
{
	phi = to_radians(phi);
	const auto c = 0.5 * profile * c_w * density / (m * g);
	const auto a = sqrt(c) * g * t;
	const auto b = atan(sin(phi) * sqrt(c) * v_0);
	const auto d = cos(b);

	const auto t_u = calc_t_u(phi, v_0, c);

	if (t <= t_u) {
		return log(cos(b - a) / d) / (c * g) + y_0;
	}
	return -(log((exp(2 * (a - b)) + 1) * d / 2) - a + b) / (c * g) + y_0;
}
