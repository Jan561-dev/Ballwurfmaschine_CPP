#pragma once
#include <exception>

namespace core {
	class point_not_reachable_exception : public std::exception
	{
	public:
		explicit point_not_reachable_exception(const char* msg);
	};
}